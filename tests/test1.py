from playwright.sync_api import Page, expect
from pages import LoginPage, HomePage, BasePage, TryForFreePage


def test_page(page: Page):
    BasePage.open_app(page)
    HomePage.accept_cookies(page)
    HomePage.click_client_access(page)
    LoginPage.login(page)
    
    

def test_try_For_Free(page: Page):
    BasePage.open_app(page)
    HomePage.accept_cookies(page)
    HomePage.open_try_for_free(page)
    # parametrize the data filler from a json file or from test file 
    TryForFreePage.dataFillerFreelance(page)