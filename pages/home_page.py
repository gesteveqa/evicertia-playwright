from locators import HomeLocators

class HomePage:

    def accept_cookies(page):
        page.get_by_role("link", name=HomeLocators.ALLOW_COOKIES_BUTTON).click()

    def click_client_access(page):
        page.get_by_title(HomeLocators.CLIENT_ACCESS_BUTTON).click()

    def open_try_for_free(page):
        page.get_by_title(HomeLocators.TRY_FOR_FREE_BUTTON).click()