from playwright.sync_api import Page
from locators import TryForFreeLocators

class TryForFreePage:
    def dataFillerFreelance(page: Page):
        page.locator(TryForFreeLocators.FRELANCE_CHECKBOX).click()
        page.locator(TryForFreeLocators.BASIC_INFORMATION_NAME).type('Gustavo')
        page.locator(TryForFreeLocators.BASIC_INFORMATION_LASTNAME).type('Esteve')
        # remove this wait, is just to check everything went ok
        page.wait_for_timeout(3000)