# pages/__init__.py

from .home_page import HomePage
from .login_page import LoginPage
from .base_page import BasePage
from .try_for_free_page import TryForFreePage
