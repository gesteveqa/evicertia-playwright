from playwright.sync_api import Page
from locators import LoginLocators

class LoginPage:
    def login(page: Page):
        page.locator(LoginLocators.USERNAME).type('Gustavo')
        page.locator(LoginLocators.PASSWORD).type('GustavoESteve')
        page.get_by_role(LoginLocators.REMEMBER_ME).click()
        page.locator(LoginLocators.LOGIN_BUTTON).click()
        # remove this wait, is just to check everything went ok
        page.wait_for_timeout(3000)


