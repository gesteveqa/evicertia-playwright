from playwright.sync_api import Locator

class BasePage:
    def open_app(page):
        page.goto("https://www.evicertia.com/en/")
