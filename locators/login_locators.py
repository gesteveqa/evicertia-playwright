# from playwright.sync_api import Locator

class LoginLocators:
    USERNAME = name= '[id="UserName"]'
    PASSWORD = '[id="Password"]'
    REMEMBER_ME = "checkbox"
    LOGIN_BUTTON = '[id="site-logon-button1"]'
   